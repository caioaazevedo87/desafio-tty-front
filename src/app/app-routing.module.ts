import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstadosComponent } from './estados/estados.component';
import { EstadoDetalheComponent } from './estado-detalhe/estado-detalhe.component';
import { EstadoEditarComponent } from './estado-editar/estado-editar.component';
import { EstadoNovoComponent } from './estado-novo/estado-novo.component';
import { CidadesComponent } from './cidades/cidades.component';
import { CidadeDetalheComponent } from './cidade-detalhe/cidade-detalhe.component';
import { CidadeNovoComponent } from './cidade-novo/cidade-novo.component';
import { CidadeEditarComponent } from './cidade-editar/cidade-editar.component';
import { EnderecosComponent } from './enderecos/enderecos.component';
import { EnderecoDetalheComponent } from './endereco-detalhe/endereco-detalhe.component';
import { EnderecoEditarComponent } from './endereco-editar/endereco-editar.component';
import { EnderecoNovoComponent } from './endereco-novo/endereco-novo.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ClienteDetalheComponent } from './cliente-detalhe/cliente-detalhe.component';
import { ClienteNovoComponent } from './cliente-novo/cliente-novo.component';
import { ClienteEditarComponent } from './cliente-editar/cliente-editar.component';


const routes: Routes = [
  {
    path: 'estados',
    component: EstadosComponent,
    data: { title: 'Lista de Estados' }
  },
  {
    path: 'estado-detalhe/:id',
    component: EstadoDetalheComponent,
    data: { title: 'Detalhe do Estado' }
  },
  {
    path: 'estado-novo',
    component: EstadoNovoComponent,
    data: { title: 'Adicionar Estado' }
  },
  {
    path: 'estado-editar/:id',
    component: EstadoEditarComponent,
    data: { title: 'Editar o Estado' }
  },
  {
    path: 'cidades',
    component: CidadesComponent,
    data: { title: 'Lista de Cidades' }
  },
  {
    path: 'cidade-detalhe/:id',
    component: CidadeDetalheComponent,
    data: { title: 'Detalhe da Cidade' }
  },
  {
    path: 'cidade-novo',
    component: CidadeNovoComponent,
    data: { title: 'Adicionar Cidade' }
  },
  {
    path: 'cidade-editar/:id',
    component: CidadeEditarComponent,
    data: { title: 'Editar o Cidade' }
  },
  {
    path: 'enderecos',
    component: EnderecosComponent,
    data: { title: 'Lista de Enderecos' }
  },
  {
    path: 'endereco-detalhe/:id',
    component: EnderecoDetalheComponent,
    data: { title: 'Detalhe da Endereco' }
  },
  {
    path: 'endereco-novo',
    component: EnderecoNovoComponent,
    data: { title: 'Adicionar Endereco' }
  },
  {
    path: 'endereco-editar/:id',
    component: EnderecoEditarComponent,
    data: { title: 'Editar o Endereco' }
  },
  {
    path: 'clientes',
    component: ClientesComponent,
    data: { title: 'Lista de Clientes' }
  },
  {
    path: 'cliente-detalhe/:id',
    component: ClienteDetalheComponent,
    data: { title: 'Detalhe da Cliente' }
  },
  {
    path: 'cliente-novo',
    component: ClienteNovoComponent,
    data: { title: 'Adicionar Cliente' }
  },
  {
    path: 'cliente-editar/:id',
    component: ClienteEditarComponent,
    data: { title: 'Editar o Cliente' }
  },
  { path: '',
    redirectTo: '/clientes',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
