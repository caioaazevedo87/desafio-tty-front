import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Estado } from 'src/model/estado';

@Component({
  selector: 'app-estados',
  templateUrl: './estados.component.html',
  styleUrls: ['./estados.component.scss']
})

export class EstadosComponent implements OnInit {

  displayedColumns: string[] = ["id", "nome", "acao"];
  dataSource: Estado[];
  constructor(private _api: ApiService) { }
  

  ngOnInit() {
    this._api.getEstados()
    .subscribe(res => {
      this.dataSource = res;
    }, err => {
      console.log(err);
    });
  }

  

}
