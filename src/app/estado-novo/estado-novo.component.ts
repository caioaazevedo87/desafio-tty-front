import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ApiService } from '../api.service';


@Component({
  selector: 'app-estado-novo',
  templateUrl: './estado-novo.component.html',
  styleUrls: ['./estado-novo.component.scss']
})
export class EstadoNovoComponent implements OnInit {

  estadoForm: FormGroup;
  isLoadingResults = false;
  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }


  ngOnInit() {
    this.estadoForm = this.formBuilder.group({
   'nome' : [null, Validators.required]
 });
 }

 addEstado(form: NgForm) {
  this.isLoadingResults = true;
  this.api.addEstado(form)
    .subscribe(res => {
        this.isLoadingResults = false;
        this.router.navigate(['/estados']);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      });
}

}
