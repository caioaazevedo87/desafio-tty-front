import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EstadosComponent } from './estados/estados.component';
import { EstadoDetalheComponent } from './estado-detalhe/estado-detalhe.component';
import { EstadoNovoComponent } from './estado-novo/estado-novo.component';
import { EstadoEditarComponent } from './estado-editar/estado-editar.component';
import { MenuComponent } from './menu/menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { CidadesComponent } from './cidades/cidades.component';
import { CidadeDetalheComponent } from './cidade-detalhe/cidade-detalhe.component';
import { CidadeNovoComponent } from './cidade-novo/cidade-novo.component';
import { CidadeEditarComponent } from './cidade-editar/cidade-editar.component';
import { EnderecosComponent } from './enderecos/enderecos.component';
import { EnderecoDetalheComponent } from './endereco-detalhe/endereco-detalhe.component';
import { EnderecoNovoComponent } from './endereco-novo/endereco-novo.component';
import { EnderecoEditarComponent } from './endereco-editar/endereco-editar.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ClienteDetalheComponent } from './cliente-detalhe/cliente-detalhe.component';
import { ClienteNovoComponent } from './cliente-novo/cliente-novo.component';
import { ClienteEditarComponent } from './cliente-editar/cliente-editar.component';

@NgModule({
  declarations: [
    AppComponent,
    EstadosComponent,
    EstadoDetalheComponent,
    EstadoNovoComponent,
    EstadoEditarComponent,
    MenuComponent,
    CidadesComponent,
    CidadeDetalheComponent,
    CidadeNovoComponent,
    CidadeEditarComponent,
    EnderecosComponent,
    EnderecoDetalheComponent,
    EnderecoNovoComponent,
    EnderecoEditarComponent,
    ClientesComponent,
    ClienteDetalheComponent,
    ClienteNovoComponent,
    ClienteEditarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,  
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatListModule,
    MatProgressSpinnerModule, 
    MatSelectModule,
    MatSidenavModule,  
    MatTableModule,
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }