import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { Estado } from 'src/model/estado';
@Component({
  selector: 'app-estado-detalhe',
  templateUrl: './estado-detalhe.component.html',
  styleUrls: ['./estado-detalhe.component.scss']
})
export class EstadoDetalheComponent implements OnInit {
  estado: Estado = { id: '', nome: '' };
  isLoadingResults = true;
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService) { }


  ngOnInit() {
    this.getEstado(this.route.snapshot.params['id']);
  }

  getEstado(id) {
    this.api.getEstado(id)
      .subscribe(data => {
        this.estado = data;
        console.log(this.estado);
        this.isLoadingResults = false;
      });
  }

  deleteEstado(id) {
    this.isLoadingResults = true;
    this.api.deleteEstado(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.router.navigate(['/estados']);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }
}