import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CidadeNovoComponent } from './cidade-novo.component';

describe('CidadeNovoComponent', () => {
  let component: CidadeNovoComponent;
  let fixture: ComponentFixture<CidadeNovoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CidadeNovoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CidadeNovoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
