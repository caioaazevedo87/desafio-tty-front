import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ApiService } from '../api.service';


@Component({
  selector: 'app-cidade-novo',
  templateUrl: './cidade-novo.component.html',
  styleUrls: ['./cidade-novo.component.scss']
})
export class CidadeNovoComponent implements OnInit {

  cidadeForm: FormGroup;
  isLoadingResults = false;
  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }


  ngOnInit() {
    this.cidadeForm = this.formBuilder.group({
   'nome' : [null, Validators.required],
   'idEstado' : [null, Validators.required]
 });
 }

 addCidade(form: NgForm) {
  this.isLoadingResults = true;
  this.api.addCidade(form)
    .subscribe(res => {
        this.isLoadingResults = false;
        this.router.navigate(['/cidades']);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      });
}

}
