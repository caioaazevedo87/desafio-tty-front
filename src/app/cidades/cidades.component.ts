import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Cidade } from 'src/model/cidade';

@Component({
  selector: 'app-cidades',
  templateUrl: './cidades.component.html',
  styleUrls: ['./cidades.component.scss']
})

export class CidadesComponent implements OnInit {

  displayedColumns: string[] = ["id", "nome", "estado", "acao"];
  dataSource: Cidade[];
  constructor(private _api: ApiService) { }
  

  ngOnInit() {
    this._api.getCidades()
    .subscribe(res => {
      this.dataSource = res;
    }, err => {
      console.log(err);
    });
  }

  

}
