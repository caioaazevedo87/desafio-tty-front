import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ApiService } from '../api.service';


@Component({
  selector: 'app-cliente-novo',
  templateUrl: './cliente-novo.component.html',
  styleUrls: ['./cliente-novo.component.scss']
})
export class ClienteNovoComponent implements OnInit {

  clienteForm: FormGroup;
  isLoadingResults = false;
  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }


  ngOnInit() {
    this.clienteForm = this.formBuilder.group({
   'nome' : [null, Validators.required],
   'email' : [null, Validators.required],
   'cpfCnpj' : [null, Validators.required],
   'tipoCliente' : [null, Validators.required],
   'dataNascimento' : [null, Validators.required],
   'endereco' : [null, Validators.required]

 });
 }

 addCliente(form: NgForm) {
  this.isLoadingResults = true;
  this.api.addCliente(form)
    .subscribe(res => {
        this.isLoadingResults = false;
        this.router.navigate(['/clientes']);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      });
}

}
