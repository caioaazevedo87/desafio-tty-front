import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoEditarComponent } from './estado-editar.component';

describe('EstadoEditarComponent', () => {
  let component: EstadoEditarComponent;
  let fixture: ComponentFixture<EstadoEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
