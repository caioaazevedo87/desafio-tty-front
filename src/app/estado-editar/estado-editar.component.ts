
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ApiService } from '../api.service';
@Component({
  selector: 'app-estado-editar',
  templateUrl: './estado-editar.component.html',
  styleUrls: ['./estado-editar.component.scss']
})
export class EstadoEditarComponent implements OnInit {
  id: String = '';
  estadoForm: FormGroup;
  nome: String = '';
  isLoadingResults = false;
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getEstado(this.route.snapshot.params['id']);
    this.estadoForm = this.formBuilder.group({
   'nome' : [null, Validators.required]
 });
 }

 getEstado(id) {
  this.api.getEstado(id).subscribe(data => {
    this.id = data.id;
    this.estadoForm.setValue({
      nome: data.nome
    });
  });
}

updateEstado(form: NgForm) {
  this.isLoadingResults = true;
  this.api.updateEstado(this.id, form)
    .subscribe(res => {
        this.isLoadingResults = false;
        this.router.navigate(['/estado-detalhe/' + this.id]);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      }
    );
}
}