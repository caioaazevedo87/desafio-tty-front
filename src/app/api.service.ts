import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Estado } from 'src/model/estado';
import { Cidade } from 'src/model/cidade';
import { Cliente } from 'src/model/cliente';
import { Endereco } from 'src/model/endereco';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrlEstado = 'http://localhost:8080/endereco/estado';
const apiUrlCidade = 'http://localhost:8080/endereco/cidade';
const apiUrlEndereco = 'http://localhost:8080/endereco/endereco-cliente';
const apiUrlCliente = 'http://localhost:8080/cliente';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getClientes (): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(apiUrlCliente)
      .pipe(
        tap(clientes => console.log('leu os clientes')),
        catchError(this.handleError('getClientes', []))
      );
  }

  getCliente(id: number): Observable<Cliente> {
    const url = `${apiUrlCliente}/${id}`;
    return this.http.get<Cliente>(url).pipe(
      tap(_ => console.log(`leu o cliente id=${id}`)),
      catchError(this.handleError<Cliente>(`getCliente id=${id}`))
    );
  }

  addCliente (cliente): Observable<Cliente> {
    return this.http.post<Cliente>(apiUrlCliente, cliente, httpOptions).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((cliente: Cliente) => console.log(`adicionou o cliente com id=${cliente.id}`)),
      catchError(this.handleError<Cliente>('addCliente'))
    );
  }

  updateCliente(id, cliente): Observable<any> {
    const url = `${apiUrlCliente}/${id}`;
    return this.http.put(url, cliente, httpOptions).pipe(
      tap(_ => console.log(`atualiza o cliente com id=${id}`)),
      catchError(this.handleError<any>('updateCliente'))
    );
  }

  deleteCliente (id): Observable<Cliente> {
    const url = `${apiUrlCliente}/${id}`;
    return this.http.delete<Cliente>(url, httpOptions).pipe(
      tap(_ => console.log(`remove o cliente com id=${id}`)),
      catchError(this.handleError<Cliente>('deleteCliente'))
    );
  }

  getEndereco(id: number): Observable<Endereco> {
    const url = `${apiUrlEndereco}/${id}`;
    return this.http.get<Endereco>(url).pipe(
      tap(_ => console.log(`leu o com id=${id}`)),
      catchError(this.handleError<Endereco>(`getEndereco id=${id}`))
    );
  }

  addEndereco (idCliente: number, endereco): Observable<Endereco> {
    const url = `${apiUrlEndereco}/${idCliente}`;
    return this.http.post<Endereco>(url, endereco, httpOptions).pipe(
      tap((endereco: Endereco) => console.log(`adicionou o cidade com id=${idCliente}`)),
      catchError(this.handleError<Endereco>('addEndereco'))
    );
  }

  updateEndereco(idCliente, idEndereco, endereco): Observable<any> {
    const url = `${apiUrlEndereco}/${idCliente}/${idEndereco}`;
    return this.http.put(url, endereco, httpOptions).pipe(
      tap(_ => console.log(`atualiza o endereco com id=${idEndereco} do cliente id=${idCliente}`)),
      catchError(this.handleError<any>('updateCidade'))
    );
  }

  getCidades (): Observable<Cidade[]> {
    return this.http.get<Cidade[]>(apiUrlCidade)
      .pipe(
        tap(cidades => console.log('leu os cidades')),
        catchError(this.handleError('getCidades', []))
      );
  }

  getCidade(id: number): Observable<Cidade> {
    const url = `${apiUrlCidade}/${id}`;
    return this.http.get<Cidade>(url).pipe(
      tap(_ => console.log(`leu o cidade id=${id}`)),
      catchError(this.handleError<Cidade>(`getCidade id=${id}`))
    );
  }

  addCidade (cidade): Observable<Cidade> {
    return this.http.post<Cidade>(apiUrlCidade, cidade, httpOptions).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((cidade: Cidade) => console.log(`adicionou o cidade com id=${cidade.id}`)),
      catchError(this.handleError<Cidade>('addCidade'))
    );
  }

  updateCidade(id, cidade): Observable<any> {
    const url = `${apiUrlCidade}/${id}`;
    return this.http.put(url, cidade, httpOptions).pipe(
      tap(_ => console.log(`atualiza o cidade com id=${id}`)),
      catchError(this.handleError<any>('updateCidade'))
    );
  }

  deleteCidade (id): Observable<Cidade> {
    const url = `${apiUrlCidade}/${id}`;
    return this.http.delete<Cidade>(url, httpOptions).pipe(
      tap(_ => console.log(`remove o cidade com id=${id}`)),
      catchError(this.handleError<Cidade>('deleteCidade'))
    );
  }

  getEstados (): Observable<Estado[]> {
    return this.http.get<Estado[]>(apiUrlEstado)
      .pipe(
        tap(estados => console.log('leu os estados')),
        catchError(this.handleError('getEstados', []))
      );
  }

  getEstado(id: number): Observable<Estado> {
    const url = `${apiUrlEstado}/${id}`;
    return this.http.get<Estado>(url).pipe(
      tap(_ => console.log(`leu o estado id=${id}`)),
      catchError(this.handleError<Estado>(`getEstado id=${id}`))
    );
  }

  addEstado (estado): Observable<Estado> {
    return this.http.post<Estado>(apiUrlEstado, estado, httpOptions).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((estado: Estado) => console.log(`adicionou o estado com id=${estado.id}`)),
      catchError(this.handleError<Estado>('addEstado'))
    );
  }

  updateEstado(id, estado): Observable<any> {
    const url = `${apiUrlEstado}/${id}`;
    return this.http.put(url, estado, httpOptions).pipe(
      tap(_ => console.log(`atualiza o estado com id=${id}`)),
      catchError(this.handleError<any>('updateEstado'))
    );
  }

  deleteEstado (id): Observable<Estado> {
    const url = `${apiUrlEstado}/${id}`;
    return this.http.delete<Estado>(url, httpOptions).pipe(
      tap(_ => console.log(`remove o estado com id=${id}`)),
      catchError(this.handleError<Estado>('deleteEstado'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log('erro de leitura')
      console.error(error);
      return of(result as T);
    };
  }
}
