import { Cidade } from './cidade';

export class Endereco {
    id: string;
    nome: string;
    logradouro: string;
	  complemento: string;
	  numero: string;
	  bairro: string;
	  cep: string;
	  cidade: Cidade;
  }