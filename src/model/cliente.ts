import { Endereco } from './endereco';

export class Cliente {
    id: string;
    nome: string;
    email: string;
    cpfCnpj: string;
    tipoCliente: string;
	  dataNascimento: string;
	  enderecos: Endereco[];
  }