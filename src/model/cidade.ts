import { Estado } from './estado';

export class Cidade {
    id: string;
    nome: string;
    estado: Estado;
  }